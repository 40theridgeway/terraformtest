

data "archive_file" "eriflambda" {
  type        = "zip"
  source_file = "scripts/exif.py"
  output_path = "exif.zip"
}

data "archive_file" "pythonlayer" {
  type        = "zip"
  source_dir = "python"
  output_path = "python.zip"
}
resource "aws_iam_policy" "erif_lambda_policy" {
  name        = "erif_lambda_policy"
  description = "erif_lambda_policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:ListBucket",
        "s3:GetObject",
        "s3:CopyObject",
        "s3:HeadObject"
      ],
      "Effect": "Allow",
      "Resource": [
                "arn:aws:s3:::data-landing-dev-20220127163408341800000002",
                "arn:aws:s3:::data-landing-dev-20220127163408341800000002/*"
      ]
    },
    {
      "Action": [
        "s3:ListBucket",
        "s3:PutObject",
        "s3:PutObjectAcl",
        "s3:CopyObject",
        "s3:HeadObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::data-repository-dev-20220127163408341700000001",
        "arn:aws:s3:::data-repository-dev-20220127163408341700000001/*"
      ]
    },
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "s3_copy_function" {
    name = "erif_lambda"

    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "lambda_iam_policy_basic_execution" {
 role = "${aws_iam_role.s3_copy_function.id}"
 policy_arn = "${aws_iam_policy.erif_lambda_policy.arn}"
}


resource "aws_lambda_permission" "allow_terraform_bucket" {
   statement_id = "AllowExecutionFromS3Bucket"
   action = "lambda:InvokeFunction"
   function_name = "${aws_lambda_function.s3_copy_function.arn}"
   principal = "s3.amazonaws.com"
   source_arn = "arn:aws:s3:::data-landing-dev-20220127163408341800000002"
}


resource "aws_lambda_function" "s3_copy_function" {
   filename = data.archive_file.eriflambda.output_path
   source_code_hash = data.archive_file.eriflambda.output_base64sha256
   function_name = "erif_lambda"
   role = "${aws_iam_role.s3_copy_function.arn}"
   handler = "exif.lambda_handler"
   runtime = "python3.8"
layers = [aws_lambda_layer_version.exif.arn]

}

resource "aws_s3_bucket_notification" "bucket_terraform_notification" {
   bucket = data.aws_s3_bucket.images.id
   lambda_function {
       lambda_function_arn = "${aws_lambda_function.s3_copy_function.arn}"
       events = ["s3:ObjectCreated:*"]
   }

   depends_on = [ aws_lambda_permission.allow_terraform_bucket ]
}
data "aws_s3_bucket" "images" {
  bucket = "data-landing-dev-20220127163408341800000002"
}

resource "aws_lambda_layer_version" "exif" {
  filename            = data.archive_file.pythonlayer.output_path
  layer_name          = "exif"
  compatible_runtimes = ["python3.8"]
}


resource "aws_iam_policy" "sources3readwritepolicy" {
  name        = "sources3readwritepolicy"
  description = "read and write policy for s3 source and destination"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::data-landing-dev-20220127163408341800000002",
                "arn:aws:s3:::data-repository-dev-20220127163408341700000001"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:DeleteObject",
                "s3:GetObject",
                "s3:GetObjectAcl",
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::data-landing-dev-20220127163408341800000002/*",
                "arn:aws:s3:::data-repository-dev-20220127163408341700000001/*"
            ]
        }
    ]
}
EOF
}


resource "aws_iam_policy" "sources3readpolicy" {
  name        = "sources3read policy for source and destination"
  description = "read policy for s3 source and destination"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::data-landing-dev-20220127163408341800000002",
                "arn:aws:s3:::data-repository-dev-20220127163408341700000001"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectAcl"
            ],
            "Resource": [
                "arn:aws:s3:::data-landing-dev-20220127163408341800000002/*",
                "arn:aws:s3:::data-repository-dev-20220127163408341700000001/*"
            ]
        }
    ]
}
EOF
}