from exif import Image

with open("./imagenodata0.jpg", "rb") as palm_1_file:
    palm_1_image = Image(palm_1_file)
    
with open("./imagenodata1.jpg", "rb") as palm_2_file:
    palm_2_image = Image(palm_2_file)
    
images = [palm_1_image, palm_2_image]
for index, image in enumerate(images):
    if len(image.list_all()) != 0 :
        status = f"contains EXIF (version ) information."
    else:
        status = "does not contain any EXIF information."
    print(f"Image {index} {status}")
    