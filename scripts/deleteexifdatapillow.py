from PIL import Image

palm_1_image = Image.open("./IMG_1.jpg")
palm_2_image = Image.open("./IMG_2.jpg")
    
images = [palm_1_image, palm_2_image]
for index, image in enumerate(images):
    
    data = image._getexif()
    if len(data) > 0:
        status = f"contains EXIF (version) information."
        name = "./imagenodata{}.jpg".format(index)
        image2 = Image.new(image.mode,image.size)
        image2.save(name)
    else:
        status = "does not contain any EXIF information."
    print(f"Image {index} {status}")

    