from PIL import Image

'''
palm_1_image = Image.open("./images/IMG_1.jpg")
palm_2_image = Image.open("./images/IMG_2.jpg")
'''
palm_1_image = Image.open("./imagenodata0.jpg")
palm_2_image = Image.open("./imagenodata1.jpg")

images = [palm_1_image, palm_2_image]
for index, image in enumerate(images):
    data = list(image.getdata())
    if len(data) > 0:
        status = "does not contain any EXIF information."
    else:
        if len(data) > 0:
            status = f"contains EXIF (version ) information."
        else:
            status = "does not contain any EXIF information."
    print(f"Image {index} {status}")
    